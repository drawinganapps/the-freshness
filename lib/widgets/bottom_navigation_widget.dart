import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_freshness/helper/color_helper.dart';

class BottomNavigationWidget extends StatelessWidget {
  const BottomNavigationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.12,
      padding: EdgeInsets.only(
          left: Get.width * 0.03,
          right: Get.width * 0.03,
          bottom: Get.height * 0.03),
      child: Container(
        decoration: BoxDecoration(
            color: ColorHelper.dark, borderRadius: BorderRadius.circular(35)),
        padding:
            EdgeInsets.only(left: Get.width * 0.03, right: Get.width * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              decoration: BoxDecoration(color: ColorHelper.primary, shape: BoxShape.circle),
              padding: EdgeInsets.all(Get.height * 0.015),
              child: const Icon(Icons.home_rounded, size: 30),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(Get.height * 0.015),
              child: Icon(Icons.sms_rounded, size: 30, color: ColorHelper.darkGrey),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(Get.height * 0.015),
              child: Badge(
                badgeContent: const Text('2', style: TextStyle(
                  fontWeight: FontWeight.w600
                )),
                badgeColor: ColorHelper.yellow,
                child: Icon(Icons.shopping_bag_rounded, size: 30, color: ColorHelper.darkGrey),
              ),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(Get.height * 0.015),
              child: Icon(Icons.person_rounded, size: 30, color: ColorHelper.darkGrey),
            ),
          ],
        ),
      ),
    );
  }
}
