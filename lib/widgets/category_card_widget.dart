import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_freshness/controller/category_controller.dart';
import 'package:the_freshness/helper/color_helper.dart';
import 'package:the_freshness/models/category_model.dart';
import 'package:the_freshness/routes/AppRoutes.dart';

class CategoryCardWidget extends StatelessWidget {
  final CategoryModel category;
  final bool isSelected;

  const CategoryCardWidget(
      {super.key, required this.category, required this.isSelected});

  @override
  Widget build(BuildContext context) {
    CategoryController controller = Get.find<CategoryController>();
    return Container(
      height: Get.height * 0.1,
      padding: EdgeInsets.only(left: Get.width * 0.03, right: Get.width * 0.03),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: isSelected ? ColorHelper.yellow : ColorHelper.secondary),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              controller.setSelectedCategory(category);
            },
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: ColorHelper.primary),
                  padding: EdgeInsets.all(Get.width * 0.03),
                  child: Image.asset('assets/logos/${category.icon}',
                      width: Get.width * 0.1),
                ),
                Container(
                  margin: EdgeInsets.only(left: Get.width * 0.03),
                  child: Text(category.name,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600)),
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.toNamed(AppRoutes.ITEMS);
            },
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: Get.width * 0.03),
                  child: Text('${category.totalItems} items',
                      style: const TextStyle(
                          fontSize: 14, fontWeight: FontWeight.w600)),
                ),
                const Icon(Icons.navigate_next)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
