import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_freshness/helper/color_helper.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 55,
      child: TextField(
        decoration: InputDecoration(
            contentPadding:
                const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(color: ColorHelper.grey, width: 2)),
            filled: true,
            fillColor: ColorHelper.primary,
            hintText: 'Fresh search',
            suffixIcon: Container(
              margin: const EdgeInsets.only(right: 15),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: ColorHelper.secondary),
              child: Icon(Icons.search, color: ColorHelper.darkGrey, size: 25),
            ),
            hintStyle: TextStyle(
                fontWeight: FontWeight.w200, color: ColorHelper.darkGrey)),
      ),
    );
  }
}
