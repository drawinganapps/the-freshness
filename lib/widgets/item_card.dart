import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_freshness/helper/color_helper.dart';
import 'package:the_freshness/models/item_model.dart';

class ItemCard extends StatelessWidget {
  final ItemModel item;

  const ItemCard({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Get.width * 0.02),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: ColorHelper.secondary,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: Container(
            width: Get.width,
            padding: EdgeInsets.all(Get.width * 0.01),
            decoration: BoxDecoration(
                color: ColorHelper.primary,
                borderRadius: BorderRadius.circular(15)),
            clipBehavior: Clip.antiAlias,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Icon(item.isFavorite
                    ? Icons.favorite_rounded
                    : Icons.favorite_border_rounded),
                Image.asset(
                  'assets/images/${item.icon}',
                  fit: BoxFit.cover,
                  height: Get.height * 0.15,
                )
              ],
            ),
          )),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.015,
                bottom: Get.height * 0.005,
                left: Get.width * 0.015),
            child: Text(item.name,
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          ),
          Container(
            margin: EdgeInsets.only(left: Get.width * 0.015),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text('\$${item.price}',
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16)),
                    const Text(' /1kg', style: TextStyle(fontSize: 14)),
                  ],
                ),
                const Icon(Icons.add_rounded)
              ],
            ),
          )
        ],
      ),
    );
  }
}
