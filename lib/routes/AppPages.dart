import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:the_freshness/controller/items_binding.dart';
import 'package:the_freshness/screens/category_screen.dart';
import 'package:the_freshness/screens/items_screen.dart';
import 'package:the_freshness/screens/welcome_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(name: AppRoutes.WELCOME, page: () => const WelcomeScreen()),
    GetPage(
        name: AppRoutes.CATEGORY,
        page: () => const CategoryScreen(),
        binding: ItemsBinding()),
    GetPage(
        name: AppRoutes.ITEMS,
        page: () => const ItemsScreen(),
        binding: ItemsBinding()),
  ];
}
