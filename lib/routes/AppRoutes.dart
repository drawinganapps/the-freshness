class AppRoutes {
  static const String WELCOME = '/welcome';
  static const String CATEGORY = '/categories';
  static const String ITEMS = '/items';
}
