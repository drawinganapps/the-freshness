import 'package:the_freshness/models/category_model.dart';

class ItemModel {
  int id;
  String name;
  String icon;
  double price;
  CategoryModel category;
  bool isFavorite;

  ItemModel(this.id, this.name, this.icon, this.price, this.category, this.isFavorite);
}
