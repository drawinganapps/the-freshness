class CategoryModel {
  int id;
  String name;
  String icon;
  int totalItems;

  CategoryModel(this.id, this.name, this.icon, this.totalItems);
}
