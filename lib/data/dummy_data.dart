import 'package:the_freshness/models/category_model.dart';
import 'package:the_freshness/models/item_model.dart';

class DummyData {
  static List<CategoryModel> categories = [
    CategoryModel(1, 'Fruits', 'fruits.png', 158),
    CategoryModel(2, 'Vegetables', 'vegetables.png', 70),
    CategoryModel(3, 'Meat', 'meat.png', 70),
    CategoryModel(4, 'Spice', 'onion.png', 130),
    CategoryModel(5, 'Sea Food', 'seafoods.png', 90),
    CategoryModel(6, 'Sweets', 'sweets.png', 38),
  ];

  static List<ItemModel> items = [
    ItemModel(1, 'Pepper', 'pepper.png', 2.4, categories[2], false),
    ItemModel(1, 'Eggplant', 'eggplant.png', 3.6, categories[2], true),
    ItemModel(1, 'Tomato', 'tomato.png', 3.2, categories[2], false),
    ItemModel(1, 'Broccoli', 'broccoli.png', 1.5, categories[2], false),
    ItemModel(1, 'Potato', 'potato.png', 4.2, categories[2], false),
    ItemModel(1, 'Spinach', 'spinach.png', 1.2, categories[2], false),
  ];
}
