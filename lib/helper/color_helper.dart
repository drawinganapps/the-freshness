import 'package:flutter/material.dart';

class ColorHelper {
  static Color primary = const Color.fromRGBO(255,255,255, 1);
  static Color secondary = const Color.fromRGBO(246, 244, 240, 1);
  static Color tertiary = const Color.fromRGBO(246, 244, 240, 1);
  static Color grey = const Color.fromRGBO(230, 233, 239, 1);
  static Color darkGrey = const Color.fromRGBO(112, 111, 129, 1);
  static Color dark = const Color.fromRGBO(0, 0, 0, 1);
  static Color yellow = const Color.fromRGBO(255, 225, 116, 1);
  static Color darkArmy = const Color.fromRGBO(89, 119, 102, 1);
}
