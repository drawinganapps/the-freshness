import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:the_freshness/controller/category_controller.dart';
import 'package:the_freshness/widgets/category_card_widget.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CategoryController>(builder: (controller) {
      return SafeArea(
          child: Column(children: [
            Container(
              padding:
              EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
              margin: EdgeInsets.only(top: Get.height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Image.asset('assets/logos/man.png', width: Get.width * 0.12),
                      Container(
                        margin: EdgeInsets.only(
                            left: Get.width * 0.01, right: Get.width * 0.01),
                        child: const Text('Hello,', style: TextStyle(fontSize: 18)),
                      ),
                      const Text('Anwar',
                          style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(Icons.search, size: 35),
                      Container(
                        margin: EdgeInsets.only(right: Get.width * 0.05),
                      ),
                      const Icon(Icons.grid_view, size: 35),
                    ],
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: Get.height * 0.04, bottom: Get.height * 0.02),
              child: Text('Categories', style: GoogleFonts.lalezar(
                  fontSize: 45
              )),
            ),
            Expanded(child: ListView(
              physics: const BouncingScrollPhysics(),
              padding: EdgeInsets.only(left: Get.width * 0.02, right: Get.width * 0.02),
              children: List.generate(controller.allCategories.length, (index) {
                final bool isSelected = controller.selectedCategory == controller.allCategories[index];
                return Container(
                  margin: EdgeInsets.only(bottom: Get.height * 0.01),
                  child: CategoryCardWidget(category: controller.allCategories[index], isSelected: isSelected),
                );
              }),
            ))
          ]));
    });
  }
}
