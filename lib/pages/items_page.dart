import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:the_freshness/controller/items_controller.dart';
import 'package:the_freshness/widgets/item_card.dart';
import 'package:the_freshness/widgets/search_widget.dart';

class ItemsPage extends StatelessWidget {
  const ItemsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ItemsController>(builder: (controller) {
      return SafeArea(
          child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            margin: EdgeInsets.only(top: Get.height * 0.01),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Icon(Icons.navigate_before_rounded, size: 35),
                Column(
                  children: [
                    const Text('Vegetables',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20)),
                    Container(
                      margin: EdgeInsets.only(top: Get.height * 0.01),
                    ),
                    const Text('70 Items', style: TextStyle(fontSize: 14)),
                  ],
                ),
                Container()
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: Get.width * 0.02, right: Get.width * 0.02),
            margin: EdgeInsets.only(top: Get.height * 0.03),
            child: const SearchWidget(),
          ),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.02, bottom: Get.height * 0.02),
            child:
                Text('New Arrivals', style: GoogleFonts.lalezar(fontSize: 35)),
          ),
          Expanded(
              child: GridView.count(
            padding: EdgeInsets.only(
                left: Get.width * 0.02, right: Get.width * 0.02),
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
            childAspectRatio: 0.7,
            physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            children: List.generate(controller.allItems.length, (index) {
              return ItemCard(item: controller.allItems[index]);
            }),
          ))
        ],
      ));
    });
  }
}
