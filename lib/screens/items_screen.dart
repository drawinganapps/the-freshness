import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_freshness/pages/items_page.dart';
import 'package:the_freshness/widgets/bottom_navigation_widget.dart';

class ItemsScreen extends StatelessWidget {
  const ItemsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: ItemsPage(),
      bottomNavigationBar: BottomNavigationWidget(),
    );
  }

}