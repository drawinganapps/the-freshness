import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_freshness/pages/category_page.dart';
import 'package:the_freshness/widgets/bottom_navigation_widget.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const CategoryPage(),
      bottomNavigationBar: const BottomNavigationWidget(),
    );
  }

}
