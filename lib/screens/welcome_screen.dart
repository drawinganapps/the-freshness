import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_freshness/helper/color_helper.dart';
import 'package:the_freshness/routes/AppRoutes.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Image.asset('assets/images/logo.png', width: Get.width * 0.5),
            Image.asset('assets/images/background.png', fit: BoxFit.cover),
            Container(
                margin: EdgeInsets.only(top: Get.height * 0.08),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                  color: ColorHelper.dark,
                  width: 2.0, // Underline thickness
                ))),
                child: GestureDetector(
                  onTap: () => Get.toNamed(AppRoutes.CATEGORY),
                  child: Text(
                    'Get Started',
                    style: TextStyle(
                      color: ColorHelper.dark,
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
