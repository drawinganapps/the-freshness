import 'package:get/get.dart';
import 'package:the_freshness/data/dummy_data.dart';
import 'package:the_freshness/models/category_model.dart';

class CategoryController extends GetxController {
  List<CategoryModel> _categories = [];
  late CategoryModel _selectedCategory;
  final _isLoading = false.obs;

  List<CategoryModel> get allCategories => _categories;
  bool get isLoading => _isLoading.value;
  CategoryModel get selectedCategory => _selectedCategory;

  @override
  void onInit() {
    super.onInit();
    initData();
  }

  void initData() async {
    _isLoading.value = true;
    await Future.delayed(const Duration(seconds: 2), () {
      _categories = DummyData.categories;
      _selectedCategory = DummyData.categories[4];
      _isLoading.value = false;
    });
    update();
  }


  void setSelectedCategory(CategoryModel category) {
    _selectedCategory = category;
    update();
  }

}
