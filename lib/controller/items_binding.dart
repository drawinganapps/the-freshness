import 'package:get/get.dart';
import 'package:the_freshness/controller/category_controller.dart';
import 'package:the_freshness/controller/items_controller.dart';

class ItemsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CategoryController>(() => CategoryController());
    Get.lazyPut<ItemsController>(() => ItemsController());
  }
}
