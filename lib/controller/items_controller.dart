import 'package:get/get.dart';
import 'package:the_freshness/data/dummy_data.dart';
import 'package:the_freshness/models/item_model.dart';

class ItemsController extends GetxController {
  List<ItemModel> _items = [];
  final _isLoading = false.obs;

  List<ItemModel> get allItems => _items;

  bool get isLoading => _isLoading.value;

  @override
  void onInit() {
    super.onInit();
    initData();
  }

  void initData() async {
    _isLoading.value = true;
    await Future.delayed(const Duration(seconds: 1), () {
      _items = DummyData.items;
      _isLoading.value = false;
    });
    update();
  }
}
